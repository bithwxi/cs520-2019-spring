(* ****** ****** *)

#include "./../assign01.dats"

(* ****** ****** *)

// Please write your code below:

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

implement
int_test() =
(
  loop(0, 1)
) where
{
fun
loop
(n: int, i: int): int =
if i = 0 then n else loop(n+1, i+i)
}

(* ****** ****** *)

val () = assertloc(int_test() = 32)

(* ****** ****** *)

implement
gheep(n) =
(
if n >= 2 then loop(1, 1, 2) else n+1
) where
{
fun
loop
( i: int
, r0: int, r1: int): int =
if
(i < n)
then loop(i+1, r1, (i+1)*r1*r0) else r1
} (* end of [gheep] *)

(* ****** ****** *)

val () =
assertloc(ghaap(5) = gheep(5))

(* ****** ****** *)

implement
intlist_append
(xs, ys) =
(
revapp
(revapp(xs, nil()), ys)
) where
{
//
fun
revapp
( xs: intlist
, ys: intlist): intlist =
(
case+ xs of
| nil() => ys
| cons(x0, xs) => revapp(xs, cons(x0, ys))
)
//
} // end of [intlist_append]

(* ****** ****** *)

(* end of [assign01_sol.dats] *)

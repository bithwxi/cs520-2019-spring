(* ****** ****** *)

#include "./../assign02.dats"

(* ****** ****** *)

// Please write your code below:

(* ****** ****** *)
//
extern
fun
{a:t@ype}
mylist_lchoose1_rest
  (xs: mylist(a))
: stream_vt(@(a, mylist(a)))
//
implement
{a}
mylist_lchoose1_rest
  (xs) = 
(
  auxlst(xs)
) where
{
//
typedef
xxs = @(a, mylist(a))
//
fun
auxlst
(
xs: mylist(a)
) : stream_vt(xxs)  = $ldelay
(
case+ xs of
| nil() =>
  (
    stream_vt_nil()
  )
| cons(x0, xs) =>
  (
    stream_vt_cons
    ( (x0, xs)
    , stream_vt_map<xxs><xxs>(auxlst(xs))
    )
  ) where
  {
    implement
    stream_vt_map$fopr<xxs><xxs>(xxs) = (xxs.0, cons(x0, xxs.1))
  } (* end of [cons] *)
)
} (* end of [mylist_lchoose1_rest] *)
//
(* ****** ****** *)

(*
fun
Power_set
(xs: mylist(int)): stream_vt(mylist(int))
*)
implement
Power_set
(xs) =
$ldelay
(
case+ xs of
| nil() =>
  sing(nil())
| cons(x0, xs) => !
  (
  Power_set(xs)
  +
  mcons(x0, Power_set(xs))
  )
) where
{
  #define
  sing
  stream_vt_sing
  fun
  mcons
  ( x0: int
  , xss
  : stream_vt(mylist(int))
  )
  : stream_vt(mylist(int)) =
  stream_vt_map_cloptr<mylist(int)><mylist(int)>(xss, lam(xs) => cons(x0, xs))
  overload + with stream_vt_append
} (* end of [Power_set] *)

(* ****** ****** *)

implement
{a}//tmp
Permutation
(xs) =
(
helper1(xs)
)
where
{
fun helper1
(
xs: mylist(a)
) : stream_vt(mylist(a)) =
(
case+ xs of
| nil() =>
  stream_vt_make_sing(nil())
| cons _ =>
  let
    val ys =
    mylist_lchoose1_rest<a>(xs)
  in
    stream_vt_concat<xs>
    (
    (
      stream_vt_map<xxs><xss>(ys)
    ) where
    {
      implement
      stream_vt_map$fopr<xxs><xss>(xxs) = mcons(xxs.0, helper1(xxs.1))
    }
    )
  end where
  {
    typedef xs = mylist(a)
    typedef xxs = @(a, mylist(a))
    vtypedef xss = stream_vt(mylist(a))
    fun
    mcons
    (
    x0: a
    ,
    xs: stream_vt(xs)
    ) : stream_vt(xs) =
    (
    stream_vt_map<xs><xs>(xs)
    ) where
    {
      implement
      stream_vt_map$fopr<xs><xs>(xs) = cons(x0, xs)
    }
  }
)
} (* end of [Permutation] *)

(* ****** ****** *)
//
val xs =
$list{int}(0, 1, 2, 3, 4)
val xs =
$UNSAFE.cast{mylist(int)}(xs)
//
val xss = Permutation<int>(xs)
val ((*void*)) =
println!("|xss| = ", stream_vt_length(xss))
//
(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [assign02_sol.dats] *)

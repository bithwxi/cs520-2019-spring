(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2019
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date:
// Thursday, the 14th of February
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

(*
//
// Here is a C program
//
// http://rosettacode.org/wiki/Power_set#C
//
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
// HX: 10 points
//
// Power_set returns a linear stream that
// contains all the possible subsequences of
// a given list. For instance, given [1, 2, 3],
// the following subsequences should be in the
// returned stream:
// [], [1], [2], [3], [1, 2], [1, 3], and [1, 2, 3] 
//
(* ****** ****** *)
//
extern
fun
Power_set(xs: mylist(int)): stream_vt(mylist(int))
extern
fun
Power_set_list(xs: mylist(int)): mylist(mylist(int))
//
(* ****** ****** *)

(*
//
// HX: 10 points
//
// Here is a C program
//
// http://rosettacode.org/wiki/Maximum_triangle_path_sum#C
//
*)
(* ****** ****** *)
//
// HX:
// Note that the returned list
// should represent the path whose
// sum is maximal among all possible path
// If there are several such paths, the
// leftmost one should be returned.
//
extern
fun
Maximum_triangle_path_sum
  (xss: mylist(mylist(int))): mylist(int)
//
val
theTriangle =
$UNSAFE.cast
{mylist(mylist(int))}
(
$list{List0(int)}
(
$list{int}(55),
$list{int}(94, 48),
$list{int}(95, 30, 96),
$list{int}(77, 71, 26, 67),
$list{int}(97, 13, 76, 38, 45),
$list{int}(7, 36, 79, 16, 37, 68),
$list{int}(48, 7, 9, 18, 70, 26, 6),
$list{int}(18, 72, 79, 46, 59, 79, 29, 90),
$list{int}(20, 76, 87, 11, 32, 7, 7, 49, 18),
$list{int}(27, 83, 58, 35, 71, 11, 25, 57, 29, 85),
$list{int}(14, 64, 36, 96, 27, 11, 58, 56, 92, 18, 55),
$list{int}(2, 90, 3, 60, 48, 49, 41, 46, 33, 36, 47, 23),
$list{int}(92, 50, 48, 2, 36, 59, 42, 79, 72, 20, 82, 77, 42),
$list{int}(56, 78, 38, 80, 39, 75, 2, 71, 66, 66, 1, 3, 55, 72),
$list{int}(44, 25, 67, 84, 71, 67, 11, 61, 40, 57, 58, 89, 40, 56, 36),
$list{int}(85, 32, 25, 85, 57, 48, 84, 35, 47, 62, 17, 1, 1, 99, 89, 52),
$list{int}(6, 71, 28, 75, 94, 48, 37, 10, 23, 51, 6, 48, 53, 18, 74, 98, 15),
$list{int}(27, 2, 92, 23, 8, 71, 76, 84, 15, 52, 92, 63, 81, 10, 44, 10, 69, 93)
)
)
//
(* ****** ****** *)
//
// HX: 20 points
//
// Permutation returns all the possible permutation
// of a given list. For instance, given [1, 2, 3],
// there are 6 permutations: [1, 2, 3], [1, 3, 2],
// [2, 1, 3], [2, 3, 1], [3, 1, 2], and, [3, 2, 1].
//
(* ****** ****** *)
//
extern
fun{a:t@ype}
Permutation(xs: mylist(a)): stream_vt(mylist(a))
extern
fun{a:t@ype}
Permutation_list(xs: mylist(a)): mylist(mylist(a))
//
(* ****** ****** *)

(* end of [assign02.dats] *)

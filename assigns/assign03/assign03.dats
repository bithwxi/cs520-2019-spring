(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2019
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Total points: 50
//
(* ****** ****** *)
//
// Due date:
// Thursday, the 21st of February
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)
//
abstype
node(a:t@ype) = ptr
typedef
nodelst
(a:t@ype) = mylist(node(a))
//
(* ****** ****** *)
//
// HX:
// this one is abstract:
//
extern
fun
{a:t@ype}
node_get_neighbors
(nx0: node(a)): nodelst(a)
//
overload .nxs with node_get_neighbors
//
(* ****** ****** *)
//
// HX-2019-02-16:
// Please implement the
// following function templates
// that do depth-first and breadth-first
// enumeration on a given graph, respectively.
// Please note that you may need to introduce
// additional *abstract* functional templates
// that you do not need to implement.
//
extern
fun
{a:t@ype}
GraphSearch_dfs
  (nx0: node(a)): stream_vt(node(a))
extern
fun
{a:t@ype}
GraphSearch_bfs
  (nx0: node(a)): stream_vt(node(a))
//
(* ****** ****** *)

(* end of [assign03.dats] *)

(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2019
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Total points: 60
//
(* ****** ****** *)
//
// Due date:
// Tuesday, the 19th of March
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

(*
//
// 20 points
//
Please do an implementation for solving
the QueenPuzzle problem that is directly based
on the function GraphSearch_dfs you implemented
in assign03.
//
For your reference, an implementation for solving
the QueenPuzzle problem is given in the following
file:
./mylib/TEST/QueenPuzzle.dats
//
*)

(* ****** ****** *)

(*
//
// 40 points
//
Please do an implementation for solving the
Knight's tour problem that is directly based
on the function GraphSearch_bfs you implemented
in assign04:
https://rosettacode.org/wiki/Knight%27s_tour
//
*)

(* ****** ****** *)

(* end of [assign04.dats] *)

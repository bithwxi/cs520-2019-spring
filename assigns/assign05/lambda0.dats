(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2019
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date:
// Tuesday, the 19th of March
//
(* ****** ****** *)
//
// Total: 80 points
//
(* ****** ****** *)
//
// 60 points
//
// Please study the code in
// lectures/lecture-03-07 and then
// implement the following version
// of lambda0 that extends the previous
// version with support for tuples
//
(* ****** ****** *)
//
// 20 points:
//
// Please implement the following term:
// TMisprime, which tests if a given integer
// is a prime number.
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

typedef
tvar = string
typedef
topr = string

(* ****** ****** *)
//
datatype
term =
| TMint of int
| TMvar of tvar
| TMlam of (tvar(*var*), term(*body*))
| TMapp of (term(*fun*), term(*arg1*))
| TMopr of (topr(*opr*), mylist(term))
| TMfix of (tvar(*f*), tvar(*x*), term(*body*))
| TMifz of (term, term, term)
//
| TMtup of mylist(term)
| TMsel of (term, int(*index*)) // starting from 0
//
(* ****** ****** *)

typedef
termlst = mylist(term)

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
fprint_val<term> = fprint_term

(* ****** ****** *)
//
// Please implement the following
// function for interpreting terms
// in lambda0:
//
extern
fun interp0 : term -> term
//
(* ****** ****** *)
//
extern
val TMisprime : term // please contruct it
//
(* ****** ****** *)

(* end of [lambda0.dats] *)

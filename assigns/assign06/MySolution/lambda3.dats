(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2019
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date:
// Thursday, the 18th of April
//
(* ****** ****** *)
//
// Total points: 40
//
(* ****** ****** *)
//
// Please implement toccurs
// for doing occurs-checking (10 pts) 
//
(* ****** ****** *)
//
// Please first gain solid understanding of the
// following code implementing tyinfer_env. Then
// try to finish the two missing cases:
//
// TMapp for 10 points and TMopr for 20 points
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

typedef
tvar = string
typedef
topr = string
typedef
tnam = string

(* ****** ****** *)

(*
datatype
Option(a:t@ype) =
None() | Some of (a)
*)

(* ****** ****** *)

datatype
type =
| TPbas of tnam
| TPfun of (type, type)
| TPtup of (type, type)
| TPref of (ref(typeopt))

where typeopt = Option(type)

typedef typelst = mylist(type)

(* ****** ****** *)

val TPint = TPbas("int")
val TPbool = TPbas("bool")
val TPstring = TPbas("string")

(* ****** ****** *)

datatype
ctype =
CTPfun of (typelst, type)

(* ****** ****** *)

fun
TPref_new
(
): type = TPref(ref(None()))

fun
TPref_get
(X: type): type =
let
val-TPref(r) = X
in
  case- r[] of
  | Some(T) => T | None() => X
end

fun
TPref_set
(X: type, T: type): void =
let
val-TPref(r) = X
in
  case- !r of
  | None() => (!r := Some(T))
end

(* ****** ****** *)

fun
teval(T: type): type =
(
case+ T of
| TPref(r) =>
  (
    case+ !r of
    | None() => T
    | Some(T) => teval(T)
  )
| _ (*non-TPref*) => T
)

(* ****** ****** *)
//
extern
val
theSig : mylist(@(topr, ctype))
//
implement
theSig = kxs where
{
//
val CTP_int_int__int =
CTPfun(cons(TPint, cons(TPint, nil())), TPint)
val CTP_int_int__bool =
CTPfun(cons(TPint, cons(TPint, nil())), TPbool)
//
val kxs = nil()
//
val kxs =
cons(("+", CTP_int_int__int), kxs)
//
val kxs =
cons(("-", CTP_int_int__int), kxs)
//
val kxs =
cons(("*", CTP_int_int__int), kxs)
val kxs =
cons(("/", CTP_int_int__int), kxs)
val kxs =
cons(("%", CTP_int_int__int), kxs)
//
val kxs =
cons(("<", CTP_int_int__bool), kxs)
val kxs =
cons((">", CTP_int_int__bool), kxs)
val kxs =
cons(("=", CTP_int_int__bool), kxs)
//
val kxs =
cons(("<=", CTP_int_int__bool), kxs)
val kxs =
cons((">=", CTP_int_int__bool), kxs)
val kxs =
cons(("!=", CTP_int_int__bool), kxs)
//
}
//
(* ****** ****** *)
//
extern
fun
theSig_find(topr): Option(ctype)
//
implement
theSig_find
  (nm) =
(
  find(theSig)
) where
{
fun
find
( kxs
: mylist(@(topr, ctype))): Option(ctype) =
(
case+ kxs of
| nil() => None()
| cons(kx, kxs) =>
  if nm = kx.0 then Some(kx.1) else find(kxs)
)
}
//
(* ****** ****** *)

extern
fun
tunify
( T1: type
, T2: type): void
extern
fun
tunify1
( T1: type
, T2: type): bool

extern
fun
toccurs
(X: type, T: type): bool

exception
TUNIFYexn of (int)

#define OCCURSCK 1
#define MISMATCH 2

implement
tunify(T1, T2) = let
//
val T1 = teval(T1)
val T2 = teval(T2)
//
in
//
case+
(T1, T2) of
| (TPref r1, _) =>
  (
  case+ T2 of
  | TPref r2 =>
    (
    if p1 = p2 then () else TPref_set(T1, T2)
    ) where
    {
      val p1 = $UNSAFE.cast{ptr}(r1)
      val p2 = $UNSAFE.cast{ptr}(r2)
    }
  | _ => if toccurs(T1, T2) then $raise(TUNIFYexn(OCCURSCK)) else TPref_set(T1, T2)
  )
| (_, TPref r2) =>
  (
  case+ T2 of
  | _ => if toccurs(T2, T1) then $raise(TUNIFYexn(OCCURSCK)) else TPref_set(T2, T1)
  )
| (TPbas nm1, TPbas nm2) => if nm1 = nm2 then () else $raise(TUNIFYexn(MISMATCH))
| (_, _) => $raise(TUNIFYexn(MISMATCH))
//
end

(* ****** ****** *)

implement
tunify1(T1, T2) =
(
try 
(tunify(T1, T2); true)
with
| ~TUNIFYexn(OCCURSCK) => false
| ~TUNIFYexn(MISMATCH) => false
)

(* ****** ****** *)

datatype
term =
| TMint of int
| TMbool of bool
| TMvar of tvar
| TMlam of (tvar(*var*), typeopt, term(*body*))
| TMapp of (term(*fun*), term(*arg1*))
| TMopr of (topr(*opr*), mylist(term))
| TMfix of (tvar(*f*), tvar(*x*), typeopt(*arg*), typeopt(*res*), term(*body*))
| TMift of (term, term, term)
| TMann of (term, type)

(* ****** ****** *)

datatype
value =
| VALint of int
| VALbool of bool
| VALlam of (term(*lam*), envir(*env*))
| VALfix of (term(*fix*), envir(*env*))
| VALtup of mylist(value)

where envir = mylist(@(tvar, value))

(* ****** ****** *)

typedef
typelst = mylist(type)
typedef
termlst = mylist(term)
typedef
valuelst = mylist(value)

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

extern
fun
print_value : (value) -> void
extern
fun
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
fprint_val<term> = fprint_term
implement
fprint_val<value> = fprint_value

(* ****** ****** *)

implement
print_term =
lam(x) => fprint_term(stdout_ref, x)
implement
prerr_term =
lam(x) => fprint_term(stderr_ref, x)
implement
fprint_term =
lam(out, t0) =>
(
case+ t0 of
//
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMbool(b) =>
  fprint!(out, "TMbool(", b, ")")
//
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, T, t) =>
  fprint!(out, "TMlam(", x, "=>", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts, ")")
| TMfix(f, x, T1, T2, t) =>
  fprint!(out, "TMfix(", f, "; ", x, "=>", t, ")")
| TMift(t1, t2, t3) =>
  fprint!(out, "TMift(", t1, "; ", t2, "; ", t3, ")")
| TMann _ =>
  fprint!(out, "TMann(", "...", ")")
)

(* ****** ****** *)

implement
print_value =
lam(x) =>
fprint_value(stdout_ref, x)
implement
prerr_value =
lam(x) =>
fprint_value(stderr_ref, x)
implement
fprint_value =
lam(out, v0) =>
(
case+ v0 of
//
| VALint(i) =>
  fprint!(out, "VALint(", i, ")")
| VALbool(b) =>
  fprint!(out, "VALbool(", b, ")")
//
| VALlam _ =>
  fprint!(out, "VALlam(", "...", ")")
| VALfix _ =>
  fprint!(out, "VALfix(", "...", ")")
//
| VALtup(vs) =>
  fprint!(out, "VALtup(", vs, ")")
//
)

(* ****** ****** *)

typedef
tenv = mylist(@(tvar, type))

(* ****** ****** *)

extern
fun
tyinfer : term -> type
static
fun
tyinfer_env : (term, tenv) -> type

(* ****** ****** *)

fun
tunopt(opt: typeopt): type =
(
case+ opt of
| None() => TPref_new() | Some(T) => T
)

(* ****** ****** *)

implement
tyinfer_env
(t0, env) =
(
case+ t0 of
//
| TMint _ => TPint
| TMbool _ => TPbool
//
| TMvar(x0) =>
  auxvar(env) where
  {
    fun
    auxvar
    (xts: tenv): type =
    (
    case- xts of
    | cons(xt, xts) =>
      if x0 = xt.0 then xt.1 else auxvar(xts)
    )
  }
//
| TMlam(x1, T1, t2) =>
  TPfun(T1, T2) where
  {
    val T1 = tunopt(T1)
    val
    env =
    cons((x1, T1), env)
    val T2 =
    tyinfer_env(t2, env)
  }
| TMfix
  ( f1, x1
  , Ta, Tr, t2) =>
  (
    Tf
  ) where
  {
//
    val Ta = tunopt(Ta)
    val Tr = tunopt(Tr)
    val Tf = TPfun(Ta, Tr)
//
    val
    env = cons((x1, Ta), env)
    val
    env = cons((f1, Tf), env)
//
    val T2 =
    tyinfer_env(t2, env)
    val-true = tunify1(T2, Tr)
//
  } (* end of [TMfix] *)
| TMift(t1, t2, t3) =>
  T2 where
  {
    val T1 = infer(t1)
    val-true = tunify1(T1, TPbool)
    val T2 = infer(t2)
    val T3 = infer(t3)
    val-true = tunify1(T2, T3)
  }
//
| TMann(t1, T1) =>
  T1 where { val-true = tunify1(T1, infer(t1)) }
//
) where
{
  macdef
  infer(x) = tyinfer_env( ,(x) , env )
}

(* ****** ****** *)

extern
fun
interp1 : term -> value

static
fun
interp1_env : (term, envir) -> value
static
fun
interp1_var : (term, envir) -> value
static
fun
interp1_opr : (term, envir) -> value

(* ****** ****** *)

implement
interp1(t0) =
interp1_env(t0, nil())

implement
interp1_env(t0, env) =
(
case+ t0 of
//
| TMint i => VALint(i)
| TMbool b => VALbool(b)
//
| TMvar _ => interp1_var(t0, env)
//
| TMlam _ => VALlam(t0, env)
| TMfix _ => VALfix(t0, env)
//
| TMapp(t1, t2) =>
  let
    val v1 = eval(t1)
    val v2 = eval(t2)
  in
    case- v1 of
    | VALlam(t1, env1) =>
      (
      interp1_env(t1, cons((x1, v2), env1))
      ) where
      {
        val-TMlam(x1, _, t1) = t1
      }
    | VALfix(t1, env1) =>
      (
      interp1_env(t1, cons((f1, v1), cons((x1, v2), env1)))
      ) where
      {
        val-TMfix(f1, x1, _, _, t1) = t1
      }
  end
//
| TMopr _ => interp1_opr(t0, env)
//
| TMift(t1, t2, t3) =>
  let
    val v1 = eval(t1)
  in
    case- v1 of
    | VALbool(b) =>
      if (b)
      then eval(t2) else eval(t3)
  end
//
| TMann(t, _) => interp1_env(t, env)
//
) where
{
  macdef
  eval(x) = interp1_env(,(x), env)
}

(* ****** ****** *)

implement
interp1_var(t0, env) =
(
let
  val-
  TMvar(x0) = t0 in auxlst(x0, env)
end
) where
{
fun
auxlst(x0: tvar, xvs: envir): value =
(
case- xvs of
(*
| nil() => ...
*)
| cons(xv, xvs) =>
  if xv.0 = x0 then xv.1 else auxlst(x0, xvs)
)
} (* end of [interp1_var] *)

(* ****** ****** *)

implement
interp1_opr(t0, env) = let
  val-
  TMopr(opr, ts) = t0
  val vs =
  (
  mylist_map<term><value>(ts)
  ) where
  {
  implement
  mylist_map$fopr<term><value>(t) = interp1_env(t, env)
  }
in
  case- opr of
  | "+" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1+i2)
    )
  | "-" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1-i2)
    )
  | "*" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1*i2)
    )
  | "/" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1/i2)
    )
  | "%" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1%i2)
    )
  | ">" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALbool(i1 > i2)
    )
  | "<" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALbool(i1 < i2)
    )
  | "=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALbool(i1 = i2)
    )
  | ">=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALbool(i1 >= i2)
    )
  | "<=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALbool(i1 <= i2)
    )
  | "!=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALbool(i1 != i2)
    )
end

(* ****** ****** *)

macdef
TMapp2(f, x1, x2) =
TMapp(TMapp(,(f), ,(x1)), ,(x2))

(* ****** ****** *)

val n = TMvar("n")
val f = TMvar("f")

(* ****** ****** *)

macdef
TMieq(x, y) =
TMopr("=", cons(,(x), cons(,(y), nil())))
macdef
TMifz(t1, t2, t3) = 
TMift(TMieq(,(t1), TMint(0)), ,(t2), ,(t3))

(* ****** ****** *)

val
TMfact =
TMfix
( "f", "n"
, None, None
, TMifz(n, TMint(1), TMopr("*", n :: TMapp(f, TMopr("-", n :: TMint(1) :: nil())) :: nil())))

(* ****** ****** *)

val TMfact10 = TMapp(TMfact, TMint(10))
val () = println! ("TMfact10 = ", interp1(TMfact10))

(* ****** ****** *)

(* end of [lambda3.dats] *)

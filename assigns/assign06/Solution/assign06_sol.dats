(* ****** ****** *)

#include "./lambda3.dats"

(* ****** ****** *)

#staload UN = $UNSAFE

implement
toccurs(X, T0) =
let
  val T0 = teval(T0)
in
  case+ T0 of
  | TPbas _ => false
  | TPfun(T1, T2) =>
    toccurs(X, T1) || toccurs(X, T2)
  | TPtup(T1, T2) =>
    toccurs(X, T1) || toccurs(X, T2)
  | TPref(r0) =>
    (
    let val-TPref(rx) = X in $UN.cast{ptr}(r0) = $UN.cast{ptr}(rx) end
    )
end // end of [toccurs]

(* ****** ****** *)

(*
local

val x = TMvar("x")

in

val
TMomega =
TMlam("x", None, TMapp(x, x))

val TPomega = tyinfer(TMomega)

end
*)

(* ****** ****** *)

(* end of [assign06_sol.dats] *)
(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2019
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Total points: 40
//
(* ****** ****** *)
//
// Due date:
// Thursday, the 2nd of May
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

dataprop
EQINT(int, int) = {x:int} EQINT(x, x)

(* ****** ****** *)
//
dataprop
FIB(int(*n*), int(*r*)) =
| FIBbas0 (0, 0)
| FIBbas1 (1, 1)
| {n:nat}{r0,r1:int}
  FIBind2 (n+2, r0+r1) of (FIB(n, r0), FIB(n+1, r1))
//
(* ****** ****** *)
//
extern
prfun // 5 points
fib_istot{n:nat}
  ((*void*)): [r:int] FIB(n, r)
//
(* ****** ****** *)
//
extern
prfun // 5 points
fib_isfun
  {n:nat}{r1,r2:int}
  ( pf1: FIB(n, r1)
  , pf2: FIB(n, r2)): EQINT(r1, r2)
//
(* ****** ****** *)
//
dataprop
FIBSUM(int(*n*), int(*s*)) =
| FIBSUMbas(0, 0)
| {n:nat}{s,r:int}
  FIBSUMind(n+1, s+r) of (FIBSUM(n, s), FIB(n, r))
//
(* ****** ****** *)
//
// Please implement lemma_fibsum
//
(* ****** ****** *)
//
(*
1 + fib(1) + fib(2) + ... + fib(n-1) = fib(n+2)
*)
//
extern
prfun // 10 points
lemma_fibsum
{n:nat}{s:int}(FIBSUM(n, s)): FIB(n+1, s+1)
//
(* ****** ****** *)

(*
** Proving:
** 1 + 3 + 5 + ... + (2n+1) = (n+1)^2
*)

(* ****** ****** *)
//
dataprop
tally_odd(int(*n*), int(*r*)) =
| tally_odd_bas (0, 0) of ()
| {n:nat}{r:int}
  tally_odd_ind(n+1, r+2*n+1) of tally_odd(n, r)
//
(* ****** ****** *)
//
extern
prfun // 10 points
lemma_tally_odd
  {n:nat}{r:int}(pf: tally_odd(n, r)): EQINT(r, n*n)
//
(* ****** ****** *)
//
// SGN (n, i) : i = (-1)^n
//
dataprop
SGN (int, int) =
  | SGNbas (0, 1)
  | {n:nat}{i:int}
    SGNind (n+1, ~i) of SGN (n, i)
// end of [SGN]
//
(* ****** ****** *)
//
// Cassini's formula:
// fib(n)*fib(n+2) + (-1)^n = (fib(n+1))^2
//
(* ****** ****** *)
//
// Please implement fib_cassini
//
extern
prfun
fib_cassini // 20 points
  {n:nat}{f0,f1,f2:int}{sgn:int}
(
  FIB(n, f0), FIB(n+1, f1), FIB(n+2, f2), SGN(n, sgn)
) : [f0*f2 + sgn == f1*f1] void
//
(* ****** ****** *)

(* end of [assign07.dats] *)

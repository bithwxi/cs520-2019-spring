
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

extern
fun f91 : int -> int
implement
f91(x) =
if x > 100 then x - 10 else f91(f91(x+11))

implement
main0() = () where
{
  val () = println!("f91(10) = ", f91(10))
  val () = println!("f91(20) = ", f91(20))
}

(* ****** ****** *)

// How to test:
// ./hello_dats
// How to compile:
// myatscc hello.dats

(* ****** ****** *)

(*
implement
main(argc, argv) = ...
*)

(* ****** ****** *)

(*
implement
main0() =
(
print("Hello");
print(", world\n");
)
*)

(* ****** ****** *)

implement
main0() =
let

val _ = print("Hello")
val _ = print(", world\n")

in
   // nothing
end

(* ****** ****** *)

(* end of [hello.dats] *)

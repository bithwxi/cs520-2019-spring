(* ****** ****** *)
//
// How to test:
// ./tally_dats
// How to compile:
// myatscc tally.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

extern
fun tally: int -> int

(* ****** ****** *)

(*
//
// This one is recursive
// but not tail-recursive:
//
implement
tally(n) =
if n > 0 then n + tally(n-1) else 0
*)
//
// This one is tail-recursive:
//
implement
tally(n) =
helper(n, 0) where
{
fun
helper(n: int, res: int): int =
  if n > 0 then helper(n-1, n+res) else res
}

#define N 1000000

implement
main0() =
println!("tally(", N, ") = ", tally(N))

(* ****** ****** *)

(* end of [tally.dats] *)

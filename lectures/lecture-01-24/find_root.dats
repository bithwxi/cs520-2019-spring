#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

extern
fun
find_root: (int -> int) -> int

implement
find_root(f) = loop(0) where
{
fun loop(state: int): int =
  if f(state) = 0 then state else loop(state+1)
}

implement
main0() =
{
val r0 = find_root(lam(x) => (x-10)*(x+11))
val () = println!("r0 = ", r0)
}

fun
int_forall
( n: int
, test: int -> bool): bool = loop(0) where
{
fun loop(i: int): bool =
  if i < n
  then
  (if test(i) then loop(i+1) else false)
  else true
}


#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

datatype
list(a:t@ype) =
| nil of ()
| cons of (a, list(a))

val xs =
cons(1, cons(2, cons(3, nil())))

fn
{a:t@ype}
length(xs: list(a)): int =
(
helper(xs)
) where
{
fun helper =
lam(xs: list(a)): int =>
case xs of
| nil() => 0
| cons(_, xs) => 1 + helper(xs)
}

(*
fun
{a:t@ype}
list_forall
(xs: list(a), test: (a) -> bool): bool
*)

implement
main0() =
{
val () = println!("|xs| = ", length<int>(xs))
}


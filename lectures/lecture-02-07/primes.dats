
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

#include
"./../../mylib/mylib.hats"

extern
fun
from : int -> stream(int)

implement
from(x) = $delay
(
stream_cons(x, from(x+1))
)

fun
sieve
(xs: stream(int)): stream(int) =
$delay
(
let
  val-
  stream_cons(x0, xs) = !xs
in
(
  stream_cons(x0, sieve(filter(xs)))
) where
{
(*
  extern
  fun filter: stream(int) -> stream(int)
*)
  fun
  filter(xs: stream(int)): stream(int) =
  $delay
  (
   let
   val-stream_cons(x1, xs) = !xs
   in
     if x1 % x0 > 0 then stream_cons(x1, filter(xs)) else !(filter(xs))
   end
  )
}
end
)

implement main0() =
{
val thePrimes = sieve(from(2))

val () = println!("thePrimes[0] = ", thePrimes[0])
val () = println!("thePrimes[1] = ", thePrimes[1])
val () = println!("thePrimes[2] = ", thePrimes[2])

val () =
(
foreach(myint(10))
) where
{
implement
myint_foreach$work<>(i) =
let
  val i = g1ofg0(i)
  val () = assertloc(i >= 0)
in
  println!("thePrimes[", i, "] = ", thePrimes[i])
end
}
}

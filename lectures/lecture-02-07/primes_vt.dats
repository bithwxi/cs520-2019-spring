
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

#include
"./../../mylib/mylib.hats"

extern
fun
from : int -> stream_vt(int)

implement
from(x) = $ldelay
(
stream_vt_cons(x, from(x+1))
)

fun
sieve
(xs: stream_vt(int)): stream_vt(int) =
$ldelay
(
let
  val-
  ~stream_vt_cons(x0, xs) = !xs
in
(
  stream_vt_cons(x0, sieve(filter(xs)))
) where
{
  fun
  filter(xs: stream_vt(int)): stream_vt(int) =
  $ldelay
  (
   let
   val-
   ~stream_vt_cons(x1, xs) = !xs
   in
     if x1 % x0 > 0 then stream_vt_cons(x1, filter(xs)) else !(filter(xs))
   end
  , ~xs
  )
}
end
, ~xs
)

(* ****** ****** *)

implement main0() =
{
//
val
thePrimes = sieve(from(2))
//
val () =
(
stream_vt_fprint
(thePrimes, stdout_ref, 100)
) where
{
implement
stream_vt_fprint$beg<>(out) = ()
implement
stream_vt_fprint$end<>(out) = fprint_newline(out)
implement
stream_vt_fprint$sep<>(out) = fprint_newline(out)
}
//
} (* end of [main0] *)


(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#include "./../../mylib/mylib.hats"
#include "./../../mylib2/mylib2.hats"
//
(* ****** ****** *)
//
// If |xs| = n,
// then |Permutation(xs)| = n!
//
extern
fun
{a:t@ype}
Permutation
(mylist(a)): mylist(mylist(a))

(* ****** ****** *)

implement
{a}
Permutation(xs) =
(
  helper(xs)
) where
{
fun
helper
(xs: mylist(a)): mylist(mylist(a)) =
(
case+ xs of
| nil() => sing(nil())
| cons _ => helper2(xs)
)
and
helper2
(xs: mylist(a)): mylist(mylist(a)) = let
//
   val ys =
   mylist_choose1_rest(xs)
   val xss =
   (
     mylist_map(ys)
   ) where
   {
     implement
     mylist_map$fopr<@(a, mylist(a))><mylist(mylist(a))>
       (y) = mylist_mcons(y.0, helper(y.1))
   }
in
  mylist_concat<mylist(a)>(xss)
end
}

(* ****** ****** *)

val xs = cons(1, cons(2, cons(3, cons(4, cons(5, nil())))))

(* ****** ****** *)

val () = println!("|Permutation(xs)| = ", length(Permutation<int>(xs)))

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [Permutation.dats] *)


(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#include "./../../mylib/mylib.hats"
#include "./../../mylib2/mylib2.hats"
//
(* ****** ****** *)

extern
fun
{a:t@ype}
mylist_nchoose
(
xs: mylist(a), n: int
) : stream_vt(mylist(a))
//
implement
{a}
mylist_nchoose
(xs, n) =
(
auxlst(xs, n)
) where
{
typedef xs = mylist(a)
vtypedef xss = stream_vt(mylist(a))
fun
auxlst
(xs: xs, n: int): xss = $ldelay
(
if
n <= 0
then stream_vt_sing(nil())
else
(
case+ xs of
| nil() =>
  stream_vt_nil()
| cons(x0, xs) => !
  (
  stream_vt_append<xs>
  (mcons(x0, auxlst(xs, n-1)), auxlst(xs, n))
  ) where
  {
  fun
  mcons
  ( x0: a
  , xss
  : stream_vt(xs)
  )
  : stream_vt(xs) =
    stream_vt_map_cloptr<xs><xs>(xss, lam(xs) => cons(x0, xs))
  }
)
)
}

(* ****** ****** *)

(* end of [mylist_nchoose.dats] *)

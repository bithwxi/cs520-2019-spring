(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

typedef
tvar = string

(* ****** ****** *)

datatype
term =
| TMvar of tvar
| TMlam of (tvar(*var*), term(*body*))
| TMapp of (term(*fun*), term(*arg*))

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

extern
fun
subst :
(term(*t0*), tvar(*x*), term(*t*)) -> term // subst t for x in t0

implement
subst(t0, x, t) =
(
case+ t0 of
| TMvar(y) =>
  if x = y then t else t0
| TMlam(y, t1) =>
  if x = y
  then t0
  else TMlam(y, subst(t1, x, t))
| TMapp(t1, t2) =>
  TMapp(subst(t1, x, t), subst(t2, x, t))
)

(* ****** ****** *)

extern
fun
interp0 : term -> term

implement
interp0(t0) =
(
case+ t0 of
| TMvar _ => t0
| TMlam _ => t0
| TMapp(t1, t2) => let
    val t1 = interp0(t1)
    val t2 = interp0(t2)
  in
    case- t1 of
    | TMlam(x1, t1) => interp0(subst(t1, x1, t2))
  end
)

(* ****** ****** *)

(* end of [lambda0.dats] *)

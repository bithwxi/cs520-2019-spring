(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

typedef
tvar = string
typedef
topr = string

(* ****** ****** *)

datatype
term =
| TMint of int
| TMvar of tvar
| TMlam of (tvar(*var*), term(*body*))
| TMapp of (term(*fun*), term(*arg1*))
| TMopr of (topr(*opr*), mylist(term))
| TMfix of (tvar(*f*), tvar(*x*), term(*body*))
| TMifz of (term, term, term)

(* ****** ****** *)

typedef
termlst = mylist(term)

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
fprint_val<term> = fprint_term

(* ****** ****** *)

extern
fun
subst :
(term(*t0*), tvar(*x*), term(*t*)) -> term // subst t for x in t0

implement
subst(t0, x, t) =
(
case+ t0 of
| TMint _ => t0
| TMvar(y) =>
  if x = y then t else t0
| TMlam(y, t1) =>
  if x = y
  then t0
  else TMlam(y, subst(t1, x, t))
| TMapp(t1, t2) =>
  TMapp(subst(t1, x, t), subst(t2, x, t))
| TMopr(opr, ts) =>
  TMopr(opr, ts) where
  {
    val ts =
    (
    mylist_map<term><term>(ts)
    ) where
    {
      implement
      mylist_map$fopr<term><term>(t1) = subst(t1, x, t)
    }
  }
| TMfix(f, y, t1) =>
  if x = f
  then t0
  else
  if x = y
  then t0
  else TMfix(f, y, subst(t1, x, t))
| TMifz(t1, t2, t3) =>
  TMifz(subst(t1, x, t), subst(t2, x, t), subst(t3, x, t))
)

(* ****** ****** *)

implement
print_term =
lam(x) => fprint_term(stdout_ref, x)
implement
prerr_term =
lam(x) => fprint_term(stderr_ref, x)
implement
fprint_term =
lam(out, t0) =>
(
case+ t0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "=>", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts, ")")
| TMfix(f, x, t) =>
  fprint!(out, "TMfix(", f, "; ", x, "=>", t, ")")
| TMifz(t1, t2, t3) =>
  fprint!(out, "TMifz(", t1, "; ", t2, "; ", t3, ")")
)

(* ****** ****** *)

extern
fun
interp0 : term -> term
static
fun
interp0_opr : (topr, termlst) -> term

implement
interp0(t0) =
(
case+ t0 of
| TMint _ => t0
| TMvar _ => t0
| TMlam _ => t0
| TMapp(t1, t2) => let
    val t1 = interp0(t1)
    val t2 = interp0(t2)
  in
    case- t1 of
    | TMlam(x1, t1) =>
      interp0(subst(t1, x1, t2))
  end
| TMopr(opr, ts) => interp0_opr(opr, ts)
| TMfix(f, x, t) => TMlam(x, subst(t, f, t0))
| TMifz(t1, t2, t3) => let
    val t1 = interp0(t1)
  in
    case+ t1 of
    | TMint(0) => interp0(t2) | _ => interp0(t3)
  end
)

(* ****** ****** *)

implement
interp0_opr
(opr, ts) = let
//
val ts =
mylist_map<term><term>(ts) where
{
implement
mylist_map$fopr<term><term>(t) = interp0(t)
}
//
in
case- opr of
| "+" =>
  (
  case- ts of
  | TMint(i1) :: TMint(i2) :: nil() => TMint(i1+i2)
  )
| "-" =>
  (
  case- ts of
  | TMint(i1) :: TMint(i2) :: nil() => TMint(i1-i2)
  )
| "*" =>
  (
  case- ts of
  | TMint(i1) :: TMint(i2) :: nil() => TMint(i1*i2)
  )
| "=" =>
  (
  case- ts of
  | TMint(i1) :: TMint(i2) :: nil() => TMint(ifval(i1=i2, 1, 0))
  )
end // end of [interp0_opr]

(* ****** ****** *)

val x = TMvar("x")
val f = TMvar("f")
val TM_0_ = TMlam("f", TMlam("x", x))
val TM_1_ = TMlam("f", TMlam("x", TMapp(f, x)))
val TM_2_ = TMlam("f", TMlam("x", TMapp(f, TMapp(f, x))))

(* ****** ****** *)

fun succ(n: term): term =
TMlam("f", TMlam("x", TMapp(f, TMapp(TMapp(n, f), x))))

val n = TMvar("n")

(*
val
TMsucc: term =
TMlam("n", TMlam("f", TMlam("x", TMapp(f, TMapp(TMapp(n, f), x)))))
*)

(* ****** ****** *)

val TMdouble = TMlam("x", TMopr("+", x :: x :: nil()))
val () = println!("TMdouble(TMint(10)) = ", interp0(TMapp(TMdouble, TMint(10))))

(* ****** ****** *)

(*
val TM_3_ =
interp0(TMapp(TMsucc, TM_2_))
val () = println!("TM_3_ = ", TM_3_)
*)

(* ****** ****** *)

val TMfact =
TMfix("f", "n", TMifz(n, TMint(1), TMopr("*", n :: TMapp(f, TMopr("-", n :: TMint(1) :: nil())) :: nil())))

(* ****** ****** *)

val TMfact10 = TMapp(TMfact, TMint(10))
val () = println! ("TMfact10 = ", interp0(TMfact10))

(* ****** ****** *)

(* end of [lambda0.dats] *)

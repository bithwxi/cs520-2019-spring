(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

typedef
tvar = string
typedef
topr = string

(* ****** ****** *)

datatype
term =
| TMint of int
| TMvar of tvar
| TMlam of (tvar(*var*), term(*body*))
| TMapp of (term(*fun*), term(*arg1*))
| TMopr of (topr(*opr*), mylist(term))
| TMfix of (tvar(*f*), tvar(*x*), term(*body*))
| TMifz of (term, term, term)
(*
| TMtup of termlst
*)

(* ****** ****** *)

datatype
value =
| VALint of int
| VALlam of (term(*lam*), envir(*env*))
| VALfix of (term(*fix*), envir(*env*))
(*
| VALtup of mylist(value)
*)

where envir = mylist(@(tvar, value))

(* ****** ****** *)

typedef
termlst = mylist(term)
typedef
valuelst = mylist(value)

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

extern
fun
print_value : (value) -> void
extern
fun
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

(* ****** ****** *)

implement
fprint_val<term> = fprint_term
implement
fprint_val<value> = fprint_value

(* ****** ****** *)

implement
print_term =
lam(x) => fprint_term(stdout_ref, x)
implement
prerr_term =
lam(x) => fprint_term(stderr_ref, x)
implement
fprint_term =
lam(out, t0) =>
(
case+ t0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "=>", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts, ")")
| TMfix(f, x, t) =>
  fprint!(out, "TMfix(", f, "; ", x, "=>", t, ")")
| TMifz(t1, t2, t3) =>
  fprint!(out, "TMifz(", t1, "; ", t2, "; ", t3, ")")
)

(* ****** ****** *)

extern
fun
interp1 : term -> value

static
fun
interp1_env : (term, envir) -> value
static
fun
interp1_var : (term, envir) -> value

(* ****** ****** *)

implement
interp1(t0) =
interp1_env(t0, nil())

implement
interp1_env(t0, env) =
(
case- t0 of
//
| TMint i => VALint(i)
//
| TMvar _ => interp1_var(t0, env)
//
| TMlam _ => VALlam(t0, env)
| TMfix _ => VALfix(t0, env)
//
| TMapp(t1, t2) =>
  let
    val v1 = eval(t1)
    val v2 = eval(t2)
  in
    case- v1 of
    | VALlam(t1, env1) =>
      (
      interp1_env(t1, cons((x1, v2), env1))
      ) where
      {
        val-TMlam(x1, t1) = t1
      }
    | VALfix(t1, env1) =>
      (
      interp1_env(t1, cons((f1, v1), cons((x1, v2), env1)))
      ) where
      {
        val-TMfix(f1, x1, t1) = t1
      }
  end
//
| TMifz(t1, t2, t3) =>
  let
    val v1 = eval(t1)
  in
    case- v1 of
    | VALint(i) =>
      if i = 0 then eval(t2) else eval(t3)
  end
//
) where
{
  macdef
  eval(x) = interp1_env(,(x), env)
}

(* ****** ****** *)

implement
interp1_var(t0, env) =
(
let
  val-
  TMvar(x0) = t0 in auxlst(x0, env)
end
) where
{
fun
auxlst(x0: tvar, xvs: envir): value =
(
case- xvs of
| cons(xv, xvs) =>
  if xv.0 = x0 then xv.1 else auxlst(x0, xvs)
)
} (* end of [interp1_var] *)

(* ****** ****** *)

(* end of [lambda1.dats] *)

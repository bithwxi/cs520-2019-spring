(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../../mylib/mylib.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

typedef
tvar = string
typedef
topr = string

(* ****** ****** *)

datatype
term =
| TMint of int
| TMvar of tvar
| TMlam of (tvar(*var*), term(*body*))
| TMapp of (term(*fun*), term(*arg1*))
| TMopr of (topr(*opr*), mylist(term))
| TMfix of (tvar(*f*), tvar(*x*), term(*body*))
| TMifz of (term, term, term)
| TMtup of mylist(term)
| TMsel of (term, int)

(* ****** ****** *)

datatype
value =
| VALint of int
| VALlam of (term(*lam*), envir(*env*))
| VALfix of (term(*fix*), envir(*env*))
| VALtup of mylist(value)

where envir = mylist(@(tvar, value))

(* ****** ****** *)

typedef
termlst = mylist(term)
typedef
valuelst = mylist(value)

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

extern
fun
print_value : (value) -> void
extern
fun
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
fprint_val<term> = fprint_term
implement
fprint_val<value> = fprint_value

(* ****** ****** *)

implement
print_term =
lam(x) => fprint_term(stdout_ref, x)
implement
prerr_term =
lam(x) => fprint_term(stderr_ref, x)
implement
fprint_term =
lam(out, t0) =>
(
case+ t0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "=>", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts, ")")
| TMfix(f, x, t) =>
  fprint!(out, "TMfix(", f, "; ", x, "=>", t, ")")
| TMifz(t1, t2, t3) =>
  fprint!(out, "TMifz(", t1, "; ", t2, "; ", t3, ")")
| TMtup _ =>
  fprint!(out, "TMtup(", "...", ")")
| TMsel _ =>
  fprint!(out, "TMsel(", "...", ")")
)

(* ****** ****** *)

implement
print_value =
lam(x) => fprint_value(stdout_ref, x)
implement
prerr_value =
lam(x) => fprint_value(stderr_ref, x)
implement
fprint_value =
lam(out, v0) =>
(
case+ v0 of
| VALint(i) =>
  fprint!(out, "VALint(", i, ")")
| VALlam _ =>
  fprint!(out, "VALlam(", "...", ")")
| VALfix _ =>
  fprint!(out, "VALfix(", "...", ")")
| VALtup(vs) =>
  fprint!(out, "VALtup(", vs, ")")
)

(* ****** ****** *)

extern
fun
interp1 : term -> value

static
fun
interp1_env : (term, envir) -> value
static
fun
interp1_var : (term, envir) -> value
static
fun
interp1_opr : (term, envir) -> value

(* ****** ****** *)

implement
interp1(t0) =
interp1_env(t0, nil())

implement
interp1_env(t0, env) =
(
case+ t0 of
//
| TMint i => VALint(i)
//
| TMvar _ => interp1_var(t0, env)
//
| TMlam _ => VALlam(t0, env)
| TMfix _ => VALfix(t0, env)
//
| TMapp(t1, t2) =>
  let
    val v1 = eval(t1)
    val v2 = eval(t2)
  in
    case- v1 of
    | VALlam(t1, env1) =>
      (
      interp1_env(t1, cons((x1, v2), env1))
      ) where
      {
        val-TMlam(x1, t1) = t1
      }
    | VALfix(t1, env1) =>
      (
      interp1_env(t1, cons((f1, v1), cons((x1, v2), env1)))
      ) where
      {
        val-TMfix(f1, x1, t1) = t1
      }
  end
//
| TMopr _ => interp1_opr(t0, env)
//
| TMifz(t1, t2, t3) =>
  let
    val v1 = eval(t1)
  in
    case- v1 of
    | VALint(i) =>
      if i = 0 then eval(t2) else eval(t3)
  end
//
| TMtup(ts) =>
  (
  VALtup(mylist_map<term><value>(ts))
  ) where
  {
  implement
  mylist_map$fopr<term><value>(t) = interp1_env(t, env)
  }
| TMsel(t1, idx) =>
  let
    val v1 = eval(t1)
  in
    case- v1 of
    | VALtup(vs) => mylist_get_at_exn<value>(vs, idx)
  end
//
) where
{
  macdef
  eval(x) = interp1_env(,(x), env)
}

(* ****** ****** *)

implement
interp1_var(t0, env) =
(
let
  val-
  TMvar(x0) = t0 in auxlst(x0, env)
end
) where
{
fun
auxlst(x0: tvar, xvs: envir): value =
(
case- xvs of
(*
| nil() => ...
*)
| cons(xv, xvs) =>
  if xv.0 = x0 then xv.1 else auxlst(x0, xvs)
)
} (* end of [interp1_var] *)

(* ****** ****** *)

implement
interp1_opr(t0, env) = let
  val-
  TMopr(opr, ts) = t0
  val vs =
  (
  mylist_map<term><value>(ts)
  ) where
  {
  implement
  mylist_map$fopr<term><value>(t) = interp1_env(t, env)
  }
in
  case- opr of
  | "+" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1+i2)
    )
  | "-" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1-i2)
    )
  | "*" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1*i2)
    )
  | "/" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1/i2)
    )
  | "%" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1%i2)
    )
  | ">" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 > i2))
    )
  | "<" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 < i2))
    )
  | "=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 = i2))
    )
  | ">=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 >= i2))
    )
  | "<=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 <= i2))
    )
  | "!=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 != i2))
    )
end

(* ****** ****** *)

macdef
TMapp2(f, x1, x2) =
TMapp(TMapp(,(f), ,(x1)), ,(x2))

(* ****** ****** *)

val n = TMvar("n")
val f = TMvar("f")

val TMfact =
TMfix("f", "n", TMifz(n, TMint(1), TMopr("*", n :: TMapp(f, TMopr("-", n :: TMint(1) :: nil())) :: nil())))

(* ****** ****** *)

val TMfact10 = TMapp(TMfact, TMint(10))
val () = println! ("TMfact10 = ", interp1(TMfact10))

(* ****** ****** *)

val xy = TMvar("xy")
val TMswap = TMlam("xy", TMtup(TMsel(xy, 1) :: TMsel(xy, 0) :: nil()))

(* ****** ****** *)

(* end of [lambda1.dats] *)

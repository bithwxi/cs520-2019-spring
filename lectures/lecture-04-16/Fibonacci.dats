
(*
fib(0) = 0 // rule 0
fib(1) = 1 // rule 1
fib(n+2) = fib(n) + fib(n-1) // for all n >= 0 // rule 2
*)

dataprop
FIB(int, int) = // FIB(n, r) iff fib(n) = r
| FIB0(0, 0) of ()
| FIB1(1, 1) of ()
| {n:nat}{r0,r1:nat}
  FIB2(n+2, r0+r1) of (FIB(n, r0), FIB(n+1, r1))

(* ****** ****** *)

prfun
fib_lemma1
{n:nat}
{r:int} .<n>.
(pf: FIB(n, r)): [r >= 0] void =
(
case+ pf of
| FIB0() => ()
| FIB1() => ()
| FIB2(pf0, pf1) =>
  { val () = fib_lemma1(pf0)
    and () = fib_lemma1(pf1) }
)

(* ****** ****** *)

extern  
fun
fib
{n:nat}
(
 n: int(n)
) : [r:int](FIB(n, r) | int(r))

implement
fib{n}(n) =
(
ifcase
| n=0 => (FIB0() | 0)
| n=1 => (FIB1() | 1)
| _(*else*) =>
  let
    val (pf0 | r0) = fib(n-2)
    val (pf1 | r1) = fib(n-1)
    prval () = fib_lemma1(pf0)
    prval () = fib_lemma1(pf1)
  in
    (FIB2(pf0, pf1) | r0 + r1)
  end
)

(* ****** ****** *)

(* end of [Fibonacci.dats] *)

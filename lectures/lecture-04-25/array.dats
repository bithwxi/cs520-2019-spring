
dataview
array_v(a:type, int, addr) =
| {l:addr}
  array_v_nil(a, 0, l) of ()
| {n:nat}{l:addr}
  array_v_cons(a, n+1, l) of (a @ l, array_v(a, n, l+1))
  
extern
fun
{a:type}
ptr_get{l:addr}(pf: !(a @ l) >> (a @ l) | p: ptr(l)): a
extern
fun
{a:type}
ptr_set{l:addr}
(pf: !(a? @ l) >> (a @ l) | p: ptr(l), x: a): void

extern
prfun
array_v_split
{a:type}
{n:int}{i:nat | i <= n}{l:addr}
(pf: array_v(a, n, l)): (array_v(a, i, l), array_v(a, n-i, l+i))

extern
prfun
array_v_unsplit
{a:type}
{n:int}{i:nat | i <= n}{l:addr}
(pf1: array_v(a, i, l), pf2: array_v(a, n-i, l+i)): (array_v(a, n, l))

fun
{a:type}
array_get_fst
{n:pos}{l:addr}
(pf: !array_v(a, n, l) | p0: ptr(l)): a =
let
  prval
  array_v_cons(pf0, pf1) = pf // pf0 : a@l
  val x0 = ptr_get<a>(pf0 | p0)
  prval () = pf := array_v_cons(pf0, pf1)
in
  x0
end

extern
fun
{a:type}
array_get_at
{n:int}{i:nat | i < n}{l:addr}
(
  pf: !array_v(a, n, l) | p0: ptr(l), i: int(i)
) : a // end-of-fun

extern
fun
add_ptr_int
{l:addr}{i:int}(ptr(l), int(i)): ptr(l+i)
overload + with add_ptr_int

implement
{a}
array_get_at
{n}{i}{l}(pf | p0, i) = let
//
prval (pf1, pf2) =
array_v_split{a}{n}{i}{l}(pf)
//
val x0 = array_get_fst<a>(pf2 | p0 + i)
//
prval () = pf := array_v_unsplit{a}{n}{i}{l}(pf1, pf2)
//
in
  x0
end

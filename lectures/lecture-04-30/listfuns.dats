
datatype
mylist(a:t@ype, int) =
| mylist_nil(a, 0) of ()
| {n:nat}
  mylist_cons(a, n+1) of (a, mylist(a, n))
  
typedef
mylist(a) = [n:nat] mylist(a, n)
  
extern
prfun
{a:t@ype}
lemma_mylist_param
{n:int}(mylist(a, n)): [n >= 0] void
  
#define nil mylist_nil
#define cons mylist_cons

extern
fun
{a:t@ype}
length{n:int}(xs: mylist(a, n)): int(n)

implement
{a}
length(xs) =
(
case+ xs of
| nil() => 0
| cons{a}{n1}(_, xs) => 1 + length<a>{n1}(xs)
)

extern
fun
{a:t@ype}
append{n1,n2:int}
( mylist(a, n1)
, mylist(a, n2)): mylist(a, n1+n2)
implement
{a}
append = lam(xs, ys) =>
(
case+ xs of
| nil() => ys
| cons(x0, xs) =>
  cons(x0, append(xs, ys))
) where
{
  prval () = lemma_mylist_param(xs)
  prval () = lemma_mylist_param(ys)
}

extern
fun
{a:t@ype}
concat{m,n:nat}
(mylist(mylist(a, n), m)): mylist(a, m*n)

implement
{a}
concat = lam(xss) =>
(
case+ xss of
| nil() => nil()
| cons(xs, xss) => append(xs, concat(xss))
)



datatype
mylist(int, int) =
| mylist_nil(0, 0) of ()
| {s:int}{n:nat}{x:int}
  mylist_cons(s+x, n+1) of (int(x), mylist(s, n))

#define nil mylist_nil
#define cons mylist_cons

extern  
fun
append :
{s1,s2:int}{n1,n2:nat}
(mylist(s1, n1), mylist(s2, n2)) -> mylist(s1+s2, n1+n2)

fun
sort
{s:int}
{n:nat}
(xs: mylist(s, n)): mylist(s, n) =
(
case+ xs of
| nil() => nil()
| cons(x0, xs) => part(x0, xs, nil(), nil())
)

and
part
{x:int}
{s,s1,s2:int}
{n,n1,n2:nat}
( x0: int(x)
, xs: mylist(s, n)
, ys: mylist(s1, n1)
, zs: mylist(s2, n2)
) : mylist(x+s+s1+s2, 1+n+n1+n2) =
(
case- xs of
| nil() =>
  append
  (sort(ys), cons(x0, sort(zs)))
| cons(x1, xs) =>
  if
  x1 <= x0
  then part(x0, xs, cons(x1, ys), zs)
  else part(x0, xs, ys, cons(x1, zs))  
)

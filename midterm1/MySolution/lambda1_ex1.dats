(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda1.sats"

(* ****** ****** *)

extern
val TMlength : term
extern
val TMfoldleft : term

(* ****** ****** *)

extern
fun
interp1 : term -> value

(* ****** ****** *)

(* end of [lambda1_ex1.dats] *)

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda1.sats"

(* ****** ****** *)

extern fun compile : term -> term

(* ****** ****** *)

(* end of [lambda1_ex2.dats] *)

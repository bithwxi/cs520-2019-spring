(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda1.sats"

(* ****** ****** *)

extern val TMqueenpuzzle : term

(* ****** ****** *)

(* end of [lambda1_ex3.dats] *)

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda1.sats"

(* ****** ****** *)

extern
val TMlength : term
extern
val TMfoldleft : term

(* ****** ****** *)

extern
fun
interp1 : term -> value

(* ****** ****** *)

implement main0() = () 

(* ****** ****** *)

typedef
termlst = mylist(term)
typedef
valuelst = mylist(value)

(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

extern
fun
print_value : (value) -> void
extern
fun
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
fprint_val<term> = fprint_term
implement
fprint_val<value> = fprint_value

(* ****** ****** *)

implement
print_term =
lam(x) => fprint_term(stdout_ref, x)
implement
prerr_term =
lam(x) => fprint_term(stderr_ref, x)
implement
fprint_term =
lam(out, t0) =>
(
case+ t0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "=>", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts, ")")
| TMfix(f, x, t) =>
  fprint!(out, "TMfix(", f, "; ", x, "=>", t, ")")
| TMifz(t1, t2, t3) =>
  fprint!(out, "TMifz(", t1, "; ", t2, "; ", t3, ")")
| TMnil() =>
  fprint!(out, "TMnil()")
| TMcons(t1, t2) =>
  fprint!(out, "TMcons(", t1, ",", t2, ")")
| TMhead(t) =>
  fprint!(out, "TMhead(", t, ")")
| TMtail(t) =>
  fprint!(out, "TMtail(", t, ")")
| TMisnil(t) =>
 fprint!(out, "TMisnil(", t, ")")
)

(* ****** ****** *)

implement
print_value =
lam(x) => fprint_value(stdout_ref, x)
implement
prerr_value =
lam(x) => fprint_value(stderr_ref, x)
implement
fprint_value =
lam(out, v0) =>
(
case+ v0 of
| VALint(i) =>
  fprint!(out, "VALint(", i, ")")
| VALlam _ =>
  fprint!(out, "VALlam(", "...", ")")
| VALfix _ =>
  fprint!(out, "VALfix(", "...", ")")
| VALlst(vs) =>
  fprint!(out, "VALlst(", vs, ")")
)

(* ****** ****** *)

extern
fun
interp1 : term -> value

static
fun
interp1_env : (term, envir) -> value
static
fun
interp1_var : (term, envir) -> value
static
fun
interp1_opr : (term, envir) -> value

(* ****** ****** *)

implement
interp1(t0) =
interp1_env(t0, nil())

implement
interp1_env(t0, env) =
(
case+ t0 of
//
| TMint i => VALint(i)
//
| TMvar _ => interp1_var(t0, env)
//
| TMlam _ => VALlam(t0, env)
| TMfix _ => VALfix(t0, env)
//
| TMapp(t1, t2) =>
  let
    val v1 = eval(t1)
    val v2 = eval(t2)
  in
    case- v1 of
    | VALlam(t1, env1) =>
      (
      interp1_env(t1, cons((x1, v2), env1))
      ) where
      {
        val-TMlam(x1, t1) = t1
      }
    | VALfix(t1, env1) =>
      (
      interp1_env(t1, cons((f1, v1), cons((x1, v2), env1)))
      ) where
      {
        val-TMfix(f1, x1, t1) = t1
      }
  end
//
| TMopr _ => interp1_opr(t0, env)
//
| TMifz(t1, t2, t3) =>
  let
    val v1 = eval(t1)
  in
    case- v1 of
    | VALint(i) =>
      if i = 0 then eval(t2) else eval(t3)
  end
//
//
| TMnil() => VALlst(nil())
//


| TMhead(t) => 
	(
		let 
			val- VALlst(v0) = eval(t)
		in
			case- v0 of cons(v0, v1) => v0
		end
	)


| TMtail(t) => 
	(
		let 
                  val-
                  VALlst(v0) = eval(t)
		in
                  case- v0 of cons(v0, v1) => VALlst(v1)
		end
	)

| TMisnil(t) =>
	let
	 	val-
                VALlst(v0) = eval(t)
	in
		case+ v0 of 
		| nil() => VALint(1) | cons _ => VALint(0)
	end


| TMcons(t0, t1) => 
	let 
		val v0 =
                interp1_env(t0, env)
		val-
                VALlst(v1) =
                interp1_env(t1, env) in VALlst(cons(v0, v1))
	end
) where
{
  macdef
  eval(x) = interp1_env(,(x), env)
}

(* ****** ****** *)

implement
interp1_var(t0, env) =
(
let
  val-
  TMvar(x0) = t0 in auxlst(x0, env)
end
) where
{
fun
auxlst(x0: tvar, xvs: envir): value =
(
case- xvs of
(*
| nil() => ...
*)
| cons(xv, xvs) =>
  if xv.0 = x0 then xv.1 else auxlst(x0, xvs)
)
} (* end of [interp1_var] *)

(* ****** ****** *)

implement
interp1_opr(t0, env) = let
  val-
  TMopr(opr, ts) = t0
  val vs =
  (
  mylist_map<term><value>(ts)
  ) where
  {
  implement
  mylist_map$fopr<term><value>(t) = interp1_env(t, env)
  }
in
  case- opr of
  | "+" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1+i2)
    )
  | "-" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1-i2)
    )
  | "*" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1*i2)
    )
  | "/" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1/i2)
    )
  | "%" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(i1%i2)
    )
  | ">" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 > i2))
    )
  | "<" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 < i2))
    )
  | "=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 = i2))
    )
  | ">=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 >= i2))
    )
  | "<=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 <= i2))
    )
  | "!=" =>
    (
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 != i2))
    )
  | "||" =>
  	(
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 != 0 || i2 != 0 ))
    )
  | "&&" =>		
  	(
    case- vs of
    | cons(VALint(i1), cons(VALint(i2), nil())) => VALint(bool2int(i1 != 0 && i2 != 0 ))
    )
  | "abs" =>
  	(
    case- vs of
    | cons(VALint(i1), nil()) => VALint(abs(i1))
    )
end


(* ****** ****** *)

macdef
TMapp2(f, x1, x2) =
TMapp(TMapp(,(f), ,(x1)), ,(x2))

val ls = TMvar("ls")
val f = TMvar("f")
val len = TMvar("len")

val TMlength =
TMapp
(
TMfix("f", "len", TMlam("ls", TMifz(TMisnil(ls), TMapp2(f, TMopr("+", len :: TMint(1) :: nil()), TMtail(ls)), len))), TMint(0)
)

(* ****** ****** *)

macdef
TMapp3(f, x1, x2, x3) =
TMapp(TMapp2(,(f), ,(x1), ,(x2)), ,(x3))

val res = TMvar("res")
val fopr = TMvar("fopr")

val TMfoldleft =
TMfix("f", "fopr", TMlam("res", TMlam("ls", TMifz(TMisnil(ls), TMapp3(f, fopr, TMapp2(fopr, res, TMhead(ls)), TMtail(ls)), res))))

(* ****** ****** *)

(* end of [lambda1_ex1.dats] *)

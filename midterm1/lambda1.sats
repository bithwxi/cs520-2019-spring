(* ****** ****** *)

#staload
"./../mylib/mylib.sats"

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)

datatype
term =
| TMint of int
| TMvar of tvar
| TMlam of (tvar(*var*), term(*body*))
| TMapp of (term(*fun*), term(*arg1*))
| TMopr of (topr(*opr*), mylist(term))
| TMfix of (tvar(*f*), tvar(*x*), term(*body*))
| TMifz of (term, term, term)
//
| TMnil of ()
  // for constructing an empty list
| TMcons of (term, term)
  // for constructing a non-empty list
//
| TMhead of (term) // obtain the head of a list
| TMtail of (term) // obtain the tail of a list
| TMisnil of (term) // check to see if a list is nil
//
(* ****** ****** *)

datatype
value =
| VALint of int
| VALlam of (term(*lam*), envir(*env*))
| VALfix of (term(*fix*), envir(*env*))
| VALlst of mylist(value)

where envir = mylist(@(tvar, value))

(* ****** ****** *)

(* end of [lambda1.sats] *)

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda3_1.sats"

(* ****** ****** *)

extern
val TMlen : term
extern
val TMmap : term

(* ****** ****** *)

extern
fun
tyinfer : term -> type
extern
fun
interp3 : term -> value

(* ****** ****** *)

(* end of [lambda3_ex1.dats] *)

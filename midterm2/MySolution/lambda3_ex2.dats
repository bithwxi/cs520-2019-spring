(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda3_2.sats"

(* ****** ****** *)

(*
extern
val TMlen : term
extern
val TMmap : term
*)

(* ****** ****** *)
//
// HX: 20 points
//
extern
fun
tyinfer : term -> type


(* ****** ****** *)
//
// HX: 30 points
//
extern
fun
interp3 : term -> value
//
(* ****** ****** *)

(* end of [lambda3_ex2.dats] *)

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include
"./../../mylib/mylib.hats"

(* ****** ****** *)

#staload "./../lambda3_2.sats"

(* ****** ****** *)
//
// HX: 10 points
//
extern
val
thePrimes : term
//
(* ****** ****** *)
//
// HX: 20 points
//
extern
val
theQueenPuzzle : term
//
(* ****** ****** *)

(* end of [lambda3_ex3.dats] *)

(* ****** ****** *)

#staload
"./../mylib/mylib.sats"

(* ****** ****** *)

typedef tvar = string
typedef topr = string
typedef tnam = string

(* ****** ****** *)

datatype
type =
| TPbas of tnam
| TPfun of (type, type)
| TPtup of (type, type)
//
| TPlist of type
//
| TPref of (ref(typeopt))
//
where typeopt = Option(type)

typedef typelst = mylist(type)

(* ****** ****** *)

datatype
ctype = CTPfun of (typelst, type)

(* ****** ****** *)

datatype
term =
//
| TMint of int
| TMbool of bool
//
| TMvar of tvar
//
| TMapp of
  (term(*fun*), term(*arg1*))
| TMopr of
  (topr(*opr*), mylist(term))
//
| TMlam of (tvar(*var*), typeopt(*arg*), term(*body*))
| TMfix of (tvar(*f*), tvar(*x*), typeopt(*arg*), typeopt(*res*), term(*body*))
//
| TMift of (term, term, term)
//
| TMnil of ()
  // for constructing an empty list
| TMcons of (term, term)
  // for constructing a non-empty list
//
| TMhead of (term) // obtain the head of a list
| TMtail of (term) // obtain the tail of a list
| TMisnil of (term) // check to see if a list is nil
//
(* ****** ****** *)
//
datatype
value =
| VALint of int
| VALlam of
  (term(*lam*), envir(*env*))
| VALfix of
  (term(*fix*), envir(*env*))
//
| VALnil of ()
| VALcons of (value, value)
//
where envir = mylist(@(tvar, value))
//
(* ****** ****** *)

(* end of [lambda3_1.dats] *)

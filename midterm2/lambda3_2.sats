(* ****** ****** *)

#staload
"./../mylib/mylib.sats"

(* ****** ****** *)

typedef tvar = string
typedef topr = string
typedef tnam = string

(* ****** ****** *)

datatype
type =
| TPbas of tnam
| TPfun of (type, type)
| TPtup of (type, type)
//
| TPlazy of type // lazy
| TPscon of type // stream_con
//
| TPref of (ref(typeopt))
//
where typeopt = Option(type)

typedef typelst = mylist(type)

(* ****** ****** *)

datatype
ctype = CTPfun of (typelst, type)

(* ****** ****** *)

datatype
term =
//
| TMint of int
| TMbool of bool
//
| TMvar of tvar
//
| TMapp of
  (term(*fun*), term(*arg1*))
| TMopr of
  (topr(*opr*), mylist(term))
//
| TMlam of (tvar(*var*), typeopt(*arg*), term(*body*))
| TMfix of (tvar(*f*), tvar(*x*), typeopt(*arg*), typeopt(*res*), term(*body*))
//
| TMift of (term, term, term)
//
| TMnil of ()
  // for constructing an empty stream_con
| TMcons of (term, term)
  // for constructing a non-empty stream_con
//
| TMcar of (term) // obtain the head of a cons
| TMcdr of (term) // obtain the tail of a cons
| TMisnil of (term) // check to see if a value is nil
//
| TMdelay of (term) | TMforce of (term)
//
(* ****** ****** *)
//
datatype
value =
| VALint of int
| VALlam of
  (term(*lam*), envir(*env*))
| VALfix of
  (term(*fix*), envir(*env*))
//
| VALlazy of ref(tmval)
//
| VALnil of ()
| VALcons of (value, value)
//
and
tmval = // for caching
| TMVAL0 of value
| TMVAL1 of (term, envir)
//
where envir = mylist(@(tvar, value))
//
(* ****** ****** *)

(* end of [lambda3_2.dats] *)

(* ****** ****** *)
(*
For Effective ATS
*)
(* ****** ****** *)
//
staload
"libats/ML/SATS/basis.sats"
staload
"libats/ML/SATS/list0.sats"
//
(* ****** ****** *)
//
extern
fun
{inp:t0ype}
{out:t0ype}
DC_solve(inp): out
//
extern
fun
{inp:t0ype}
DC_base_test(x0: inp): bool
//
extern
fun
{inp:t0ype}
{out:t0ype}
DC_base_solve(x0: inp): out
//
(* ****** ****** *)
//
extern
fun
{inp:t0ype}
DC_divide(x0: inp): list0(inp)
//
(* ****** ****** *)
//
extern
fun
{inp:t0ype}
{out:t0ype}
DC_conquer
  (x0: inp, xs: list0(inp)): out
//
extern
fun
{inp:t0ype}
{out:t0ype}
DC_conquer_combine
  (x0: inp, rs: list0(out)): out
//
(* ****** ****** *)

implement
{inp}{out}
DC_solve
  (x0) = let
//
val
test =
DC_base_test<inp>(x0)
//
in (* in-of-let *)
//
if
(test)
then
DC_base_solve<inp>(x0)
else r0 where
{
  val xs = DC_divide<inp>(x0)
  val r0 = DC_conquer<inp><out>(x0, xs)
} (* end of [else] *)
//
end // end of [DC_solve]

(* ****** ****** *)
//
implement
{inp}{out}
DC_conquer
  (x0, xs) = r0 where
{
//
val rs =
list0_map<inp><out>
( xs
, lam(x) => DC_solve<inp><out>(x)
) (* end of [val] *)
//
val r0 = DC_conquer_combine<inp><out>(x0, rs)
//
} (* end of [DC_conquer] *)
//
(* ****** ****** *)

(* end of [DivideConquer.dats] *)

(* ****** ****** *)
(*
** DivideConquer:
** Fibonacci numbers
**
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
#staload
"./../DATS/DivideConquer.dats"
//
(* ****** ****** *)

typedef inp = int
typedef out = int

(* ****** ****** *)
//
extern
fun
Fibonacci(inp): out
//
(* ****** ****** *)
//
implement
DC_base_test<inp>
  (n) =
(
if n >= 2 then false else true
)
//
(* ****** ****** *)
//
implement
DC_base_solve<inp><out>
  (n) = n
//
(* ****** ****** *)
//
implement
DC_divide<inp>(n) =
(
g0ofg1($list{int}(n-1, n-2))
) (* DC_divide *)
//
(* ****** ****** *)

implement
DC_conquer_combine<inp><out>
  (_, rs) = r1 + r2 where
{
//
val-list0_cons(r1, rs) = rs
val-list0_cons(r2, rs) = rs
//
}

(* ****** ****** *)

implement
Fibonacci(n) = let
//
val () =
println!
(
  "Fibonacci(", n, ")"
)
in
  DC_solve<inp><out>(n)
end // end of [Fibonacci]

(* ****** ****** *)

implement
main0() =
{
//
val () =
println! ("Fibonacci(5) = ", Fibonacci(5))
val () =
println! ("Fibonacci(10) = ", Fibonacci(10))
val () =
println! ("Fibonacci(20) = ", Fibonacci(20))
val () =
println! ("Fibonacci(30) = ", Fibonacci(30))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [Fibonacci.dats] *)

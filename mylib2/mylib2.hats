
(* ****** ****** *)

(*
extern
fun
{a:t@ype}
mylist_reverse
(xs: mylist(a)): mylist(a)

implement
{a}
mylist_reverse
  (xs) = 
  mylist_revapp<a>(xs, nil())
*)
  
(* ****** ****** *)

extern
fun{a:t@ype}
mylist_head_exn
(xs: mylist(a)): a
extern
fun{a:t@ype}
mylist_tail_exn
(xs: mylist(a)): mylist(a)

overload head with mylist_head_exn
overload tail with mylist_tail_exn

(* ****** ****** *)

implement
{a}
mylist_head_exn(xs) =
let val-cons(x0, xs) = xs in x0 end
implement
{a}
mylist_tail_exn(xs) =
let val-cons(x0, xs) = xs in xs end

(* ****** ****** *)

(* end of [mylib2.hats] *)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
#include "./../mylib/mylib.hats"
//
(* ****** ****** *)

%{^
#include <math.h>
%}
val
pow_2_10 =
$extfcall
(double, "pow", 2, 10)
val ((*void*)) =
println!("2^10 = ", pow_2_10)

(* ****** ****** *)

#include "./csvread.dats"

(* ****** ****** *)
//
#define
IRIS_DATA "./DATA/iris_data.csv"
//
(* ****** ****** *)
//
val
IRIS_TABLE =
mytable where
{
//
val-
~Some_vt(inp) =
fileref_open_opt
(IRIS_DATA, file_mode_r)
//
val
mytable =
dframe_read_fileref(inp)
//
val () = fileref_close(inp)
//
} (* end of [val] *)
//
(* ****** ****** *)

typedef real = double

(* ****** ****** *)

typedef row_t =
$tup
(real, real, real, real, string)

(* ****** ****** *)

(*
fun{
a:vt0p}{b:vt0p
} stream_vt_map
  (xs: stream_vt(INV(a))): stream_vt(b)
fun{
a:vt0p}{b:vt0p
} stream_vt_map$fopr(x: &a >> a?!):<1> (b)
*)
val
the_row_list =
let
fun
trans(x0: gvhashtbl): row_t =
let
fun
real(gv: gvalue): real =
let
val-GVstring(rep) = gv
in
g0string2float_double(rep)
end
in
$tup
(
real(x0["sepal_length"])
, real(x0["sepal_width"])
,
real(x0["petal_length"])
, real(x0["petal_width"])
,
GVstring_uncons(x0["species"])
)
end
in
array0_foldright<gvhashtbl><mylist(row_t)>
(IRIS_TABLE, lam(x0, ys) => cons(trans(x0), ys), nil())
end // end of [let]

(* ****** ****** *)

extern
fun
fprint_row: fprint_type(row_t)
implement
fprint_row(out, r0) =
(
  fprint!(out, "(", r0.0, ", ", r0.1, ", ", r0.2, ", ", r0.3, ", ", r0.4, ")")
)
overload fprint with fprint_row

(* ****** ****** *)

val () =
(
mylist_foreach(the_row_list)
) where
{
implement
mylist_foreach$work<row_t>(x0) = fprintln!(stdout_ref, x0)
}

(* ****** ****** *)

implement main0 ((*void*)) = ()

(* ****** ****** *)

(* end of [mymain.dats] *)
